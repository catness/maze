#!/usr/bin/python3
"""
    Generate maze cell images based on the cell values (according to the MazeGenerator.kt algorithm).

    To update the images in the app:

    cp cell* ../app/src/main/res/drawable/
""" 
from PIL import Image

tile = Image.open("tile.png")
empty = Image.open("empty.png")
w, h = tile.size

cols = 7
rows = 7
width = w*cols
height = h*rows


def create_image(num):
    out = Image.new("RGBA", (width,height))
    for col in range(cols):
        for row in range(rows):
            x = row*w
            y = col*h
            out.paste(empty, (x,y))

    if num & 1 == 0:
        y = 0
        for row in range(rows):
            x = row*w
            out.paste(tile, (x,y))

    if num & 2 == 0:
        y = (rows-1)*h
        for row in range(rows):
            x = row*w
            out.paste(tile, (x,y))

    if num & 4 == 0:
        x = (cols-1)*w
        for col in range(cols):
            y = col*h
            out.paste(tile, (x,y))

    if num & 8 == 0:
        x = 0
        for col in range(cols):
            y = col*h
            out.paste(tile, (x,y))

    out.save("cell{}.png".format(num))

for i in range(1,15):
    create_image(i)






