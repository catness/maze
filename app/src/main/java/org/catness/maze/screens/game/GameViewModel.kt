/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.catness.maze.screens.game

import android.os.CountDownTimer
import android.text.format.DateUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

data class Item(var name: String, var image: String)

/**
 * ViewModel containing all the logic needed to run the game
 */
class GameViewModel : ViewModel() {
    private val timer: CountDownTimer

    companion object {

        // Time when the game is over
        private const val DONE = 0L

        // Countdown time interval
        private const val ONE_SECOND = 1000L

        // Total time for the game
        private const val COUNTDOWN_TIME = 60000L*5 // 5 min
    }

    private val items: List<Item> = listOf(
        Item("a key","key_a_gold"),
        Item("a key","key_b"),
        Item("an apple","apple_red_with_leaf"),
        Item("an apple","apple_green_with_leaf"),
        Item("a golden coin","coin_gold"),
        Item("a silver coin","coin_silver"),
        Item("a potion","blue_potion"),
        Item("a potion","green_potion"),
        Item("a potion","red_potion"),
        Item("a potion","violet_potion"),
        Item("a potion","yellow_potion"),
        Item("meat","meat")
        )

    private val itemsMap: MutableMap<Int, Item> = mutableMapOf()

    // The current score
    private val _score = MutableLiveData<Int>()
    val score: LiveData<Int>
        get() = _score

    // win is true if we found all the items
    private val _win = MutableLiveData<Boolean>()
    val win: LiveData<Boolean>
        get() = _win

    // message - either about finding an item, or if we can't move in that direction
    private val _message = MutableLiveData<String>()
    val message: LiveData<String>
        get() = _message

    // Countdown time
    private val _currentTime = MutableLiveData<Long>()
    val currentTime: LiveData<Long>
        get() = _currentTime


    // The String version of the current time
    val currentTimeString = Transformations.map(currentTime) { time ->
        DateUtils.formatElapsedTime(time)
    }

    // Event which triggers the end of the game
    private val _eventGameFinish = MutableLiveData<Boolean>()
    val eventGameFinish: LiveData<Boolean>
        get() = _eventGameFinish

    // The value of the current maze cell, which determines the way it's drawn
    private val _cell = MutableLiveData<Int>()
    val cell: LiveData<Int>
        get() = _cell

    // maze dimensions
    private val width = 8
    private val height = 8
    private val gen : MazeGenerator = MazeGenerator(width, height)

    // player's current location
    var cur_x : Int = 0
    var cur_y : Int = 0

    // I'm tired of the secure way of defining LiveData with separate getters. 
    // Let these be regular, for a change

    // To display current location on the screen. (We can use cur_x and cur_y and make them Live Data
    // but there's too much typing because we have to refer to them with .value)
    val x = MutableLiveData<Int>()
    val y = MutableLiveData<Int>()

    // current found item (usually null)
    val cur_item = MutableLiveData<Item>()
  
    init {
        reset()
        // Creates a timer which triggers the end of the game when it finishes
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {

            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = millisUntilFinished/ONE_SECOND
            }

            override fun onFinish() {
                _currentTime.value = DONE
                onGameFinish()
            }
        }
        timer.start()
    }

    private fun reset() {
        Log.i("GameViewModel","reset")
        _score.value = 0
        _win.value = false
        _message.value = "Start"
        cur_x = 0
        cur_y = 0
        x.value = 0
        y.value = 0

        initItems()
        gen.reset()
        gen.display(cur_x, cur_y)
        _cell.value = gen.get(cur_x, cur_y)
        Log.i("GameViewModel","cell=${_cell.value}")
        cur_item.value = null       
    }

    private fun initItems() {
        // place all the items randomly around the maze
        // we don't need to pick the item (x,y) coordinates, it's the location number instead
        // which will be calculated as y*width+x
        itemsMap.clear()
        val s: MutableSet<Int> = mutableSetOf()
        val length = width*height-1
        while (s.size < items.size) { s.add((1..length).random()) }
        val randomList = s.toList()
        items.forEachIndexed({i, item -> itemsMap[randomList[i]] = item})

        // for debugging : to be able to find the first item right away
        // itemsMap[1] = items[2]
        // itemsMap[8] = items[4]
    }

    /**
     * Callback called when the ViewModel is destroyed
     */
    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }

    /** Methods for updating the UI **/
    fun onReset() {
        reset()
        timer.cancel()
        timer.start()
    }

    fun goMaze(dir: Direction) {
        var ret = gen.move(cur_x, cur_y, dir)
        if (ret.first) {
            cur_x = ret.second
            cur_y = ret.third
            _cell.value = gen.get(cur_x, cur_y)
            gen.display(cur_x, cur_y)
            x.value = cur_x
            y.value = cur_y
            var key = cur_y*width + cur_x
            if (itemsMap.contains(key)) {
                var item = itemsMap[key]
                _message.value = "You found ${item?.name} !"
                cur_item.value = item
                itemsMap.remove(key)
                _score.value = (_score.value)?.plus(1)
                if (itemsMap.size==0) {
                    _win.value = true
                    onGameFinish()
                }
            }
            else {
                _message.value = ""
                cur_item.value = null
            }
        }
        else {
            Log.i("ViewModel","Can't move to {$dir}.")
            _message.value = "Can't move to $dir."
        }
    }

    fun goWest() {
        goMaze(Direction.W)
    }

    fun goEast() {
        goMaze(Direction.E)
    }

    fun goNorth() {
        goMaze(Direction.N)
    }

    fun goSouth() {
        goMaze(Direction.S)
    }

    /** Method for the game completed event **/

    fun onGameFinishComplete() {
        _eventGameFinish.value = false
    }

    fun onGameFinish() {
        _eventGameFinish.value = true
    }

}
