/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.catness.maze.screens.game

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment.findNavController
import org.catness.maze.R
import org.catness.maze.databinding.GameFragmentBinding
import android.graphics.drawable.Drawable
import android.widget.ImageView

/**
 * Fragment where the game is played
 */
class GameFragment : Fragment() {

    private lateinit var binding: GameFragmentBinding

    private lateinit var viewModel: GameViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate view and obtain an instance of the binding class
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.game_fragment,
                container,
                false
        )

        // Just remember the syntax... it's called this way, whatever it does
        viewModel = ViewModelProvider(this).get(GameViewModel::class.java)

        // Set the viewModel for databinding - this allows the bound layout access
        // to all the data in the VieWModel
        binding.gameViewModel = viewModel

        // Specify the fragment view as the lifecycle owner of the binding.
        // This is used so that the binding can observe LiveData updates
        binding.lifecycleOwner = viewLifecycleOwner

        // Observer for the Game finished event
        viewModel.eventGameFinish.observe(viewLifecycleOwner, Observer<Boolean> { hasFinished ->
            if (hasFinished) gameFinished()
        })

        // Observer to redraw the cell as we move around the maze
        // We can't refer to drawables from ViewModel (theoretically it's possible but not recommended)
        // So all the drawable logics has to be in the fragment
        viewModel.cell.observe(viewLifecycleOwner, Observer<Int> { num ->
            // but at least we can dynamically generate the drawable name
            var uri: String = "@drawable/cell"+ num.toString()
            Log.i("GameFragment","new cell: $uri")

            activity?.applicationContext?.let { it -> 
                var imageResource: Int = it.getResources ().getIdentifier(uri, null, it.getPackageName())
                binding.cell.setImageResource(imageResource)
            }
        })

        // Observer to draw the current item if there's an item at current location
        viewModel.cur_item.observe(viewLifecycleOwner, Observer<Item> { item ->
            if (item == null) {
                binding.item.visibility = View.GONE
            }
            else {
                var uri: String = "@drawable/"+ item.image
                activity?.applicationContext?.let { it -> 
                    var imageResource: Int = it.getResources ().getIdentifier(uri, null, it.getPackageName())
                    binding.item.setImageResource(imageResource)
                    binding.item.visibility = View.VISIBLE
                }
            }
        })

        return binding.root
    }

    /**
     * Called when the game is finished
     */
    private fun gameFinished() {
        Toast.makeText(activity, "Game has just finished", Toast.LENGTH_SHORT).show()
        val action = GameFragmentDirections.actionGameToScore()
        action.score = viewModel.score.value?:0
        action.win = viewModel.win.value?:false
        findNavController(this).navigate(action)
        viewModel.onGameFinishComplete()
    }
}
