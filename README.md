# About the project

**Maze** is an example for [Android Kotlin Fundamentals: codelabs 05.1-4 - LiveData, ViewModel, Observers & Data Binding](https://codelabs.developers.google.com/codelabs/kotlin-android-training-live-data/index.html). The code is based on GuessTheWord app, but instead of guessing words, the player has to navigate a dynamically generated maze and collect all the items, randomly scattered around the maze. The game ends either when the player collects all the items, or when the timer elapses.

Item images are from [Super Pixel Objects and Items](https://untiedgames.itch.io/super-pixel-objects-and-items). Maze tiles (just one for the time being) are from [VectorPixelStar Textures](https://vectorpixelstar.itch.io/textures). The maze cell images are generated with a Python script.

The apk is here: [maze.apk](apk/maze.apk).

The app only works in portrait mode. I didn't bother with creating an additional layout for landscape.

Bonus: a [text-only maze](console_maze) I used to test generating the maze and navigating it until getting to the opposite end. (The maze generator code is adapted from [RosettaCode](https://rosettacode.org/wiki/Maze_generation#Kotlin). ) It requires [command line Kotlin compiler](https://kotlinlang.org/docs/tutorials/command-line.html) to build it.
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)



