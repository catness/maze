


fun main(args: Array<String>) {
    val width = if (args.size >= 1) args[0].toInt() else 8
    val height = if (args.size == 2) args[1].toInt() else 8
    var end : Boolean

    val gen : MazeGenerator = MazeGenerator(width, height)
    gen.generate(0,0)
    var cur_x = 0
    var cur_y = 0
    gen.display(cur_x, cur_y)

    val dirs : List<String> = listOf("E","S","W","N")
    do {
        var input: String?
        do {
            println("Please enter direction - one of E,S,W,N:")
            input = readLine()?.toUpperCase()
        } while (input !in dirs)
        println(input)

        var dir: Direction = when(input) {
            "E" -> Direction.E
            "S" -> Direction.S
            "W" -> Direction.W
            "N" -> Direction.N
            else -> Direction.N
        }

        var ret = gen.move(cur_x, cur_y, dir)
        println(ret)
        cur_x = ret.second
        cur_y = ret.third
        gen.display(cur_x, cur_y)
        var cell = gen.get(cur_x, cur_y)
        println("Cell=$cell cur_x=$cur_x cur_y=$cur_y")

        end = (cur_x == width-1 && cur_y == height-1) 
    } while(!end)

    println("You found the exit!")
}
