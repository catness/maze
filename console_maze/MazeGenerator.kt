import java.util.*

// https://rosettacode.org/wiki/Maze_generation#Kotlin

enum class Direction(val bit: Int, val dx: Int, val dy: Int) {
    N(1, 0, -1), S(2, 0, 1), E(4, 1, 0),W(8, -1, 0);

    var opposite: Direction? = null

    companion object {
        init {
            N.opposite = S
            S.opposite = N
            E.opposite = W
            W.opposite = E
        }
    }
}


class MazeGenerator(val x: Int, val y: Int) {
    private val maze = Array(x) { IntArray(y) }
 
    fun generate(cx: Int, cy: Int) {
        Direction.values().shuffle().forEach {
            val nx = cx + it.dx
            val ny = cy + it.dy
            if (between(nx, x) && between(ny, y) && maze[nx][ny] == 0) {
                maze[cx][cy] = maze[cx][cy] or it.bit
                maze[nx][ny] = maze[nx][ny] or it.opposite!!.bit
                generate(nx, ny)
            }
        }
    }
 
    fun display(cur_x: Int, cur_y: Int) {
        for (i in 0..y - 1) {
            // draw the north edge
            for (j in 0..x - 1)
                print(if (maze[j][i] and 1 == 0) "+---" else "+   ")
            println('+')
 
            // draw the west edge
            for (j in 0..x - 1) {
                if (j==cur_x && i==cur_y) {
                    print(if (maze[j][i] and 8 == 0) "| x " else "  x ")    
                }
                else {
                    print(if (maze[j][i] and 8 == 0) "|   " else "    ")
                }
            }
            println('|')
        }
 
        // draw the bottom line
        for (j in 0..x - 1) print("+---")
        println('+')
    }
 
    inline private fun <reified T> Array<T>.shuffle(): Array<T> {
        val list = toMutableList()
        Collections.shuffle(list)
        return list.toTypedArray()
    }
 
    private fun between(v: Int, upper: Int) = v >= 0 && v < upper

    fun get(x: Int, y: Int) : Int {
        // get the numerical value of the cell at position x,y
        return maze[x][y]
    }
    
    fun move(x: Int, y: Int, d: Direction) : Triple<Boolean,Int,Int> {
        println("Move: x=$x y=$y d=$d")
        println("Maze= " + maze[x][y] + " bit=" + d.bit)
        if ( (maze[x][y] and d.bit) != 0) { // ok to move
            return Triple(true, x+d.dx, y+d.dy)
        }
        else {
            return Triple(false,x,y)
        }
    }

}